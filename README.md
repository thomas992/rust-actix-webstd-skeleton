# What does HTTP stand for?

Hyper-text-transfer-protocol

# What is Rust?

Rust is a *system level programming language* like C++. Rust files are stored in **.ru** files and compile to an executable format. Rust is quickly becoming the new standard for the computer's operations with contributions from [Mozilla](https://www.mozilla.org/en-US/), and Google. Interesting projects include [Servo, Mozilla's next generation web browser.](https://github.com/servo/servo) [Google is making a next generation text-editor with Rust.](https://github.com/google/xi-editor)

# What is WebSTD?

Rust is compiled to WebAseembly **.wasm** files. Quoting WASM's documentation site.

> WebAssembly is designed to maintain the versionless, feature-tested, and backwards-compatible nature of the web. WebAssembly modules will be able to call into and out of the JavaScript context and access browser functionality through the same Web APIs accessible from JavaScript. WebAssembly also supports non-web embeddings.

# What is Actix-web?

Actix-web is a HTTP framework. It's there when you need it.

# Why?

[Compared with Node.JS, Actix compariably runs 8x faster.](https://www.techempower.com/benchmarks/#section=data-r16&hw=ph&test=plaintext) WebGL through WebStandard brings performance up to native application.

# Documentation in wiki